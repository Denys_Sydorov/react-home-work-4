import React, { useState, useEffect } from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';
import Service from '../../services/service';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


import './app.css';

const App = () => {
  const propertyService = new Service();
/*   const [test, setTest] = useState(null);
  useEffect(() => {
    new Service().getPeoples()
      .then((data) => { setTest(data.results) })
      .catch((massege) => { console.error(massege) })

      new Service().getPlanets()
      .then((data) => { setTest(data.results) })
      .catch((massege) => { console.error(massege) })
      
      new Service().getStarships()
      .then((data) => { setTest(data.results) })
      .catch((massege) => { console.error(massege) })

  }, []); */

  return (
    <Router>
      <Header />
      <RandomPlanet />

      <div className="row mb2">
        <Route exact path='/people'>
          <div className="col-md-6">
            <ItemList /* test={test} */ getProperty={propertyService.getPeoples.bind(propertyService)} key='1' />
          </div>
        </Route>
        <Route path='/planets'>
          <div className="col-md-6">
            <ItemList /* test={test} */ getProperty={propertyService.getPlanets.bind(propertyService)} key='2' />
          </div>
        </Route>
        <Route path='/starships'>
          <div className="col-md-6">
            <ItemList /* test={test} */ getProperty={propertyService.getStarships.bind(propertyService)} key='3' />
          </div>
        </Route>
        <div className="col-md-6">
          <PersonDetails />
        </div>
      </div>
    </Router>
  );
};

export default App;