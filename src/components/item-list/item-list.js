import React, { useState, useEffect } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import './item-list.css';

const ItemList = ({ getProperty }) => {
  const [property, setProperty] = useState([])
  useEffect(() => {
    async function loadProperty() {
    const {results} = await getProperty();
    setProperty(results) //зберігаємо нові дані
    }
    if (!property.length) {
      loadProperty();
    }
  }, [getProperty])
  return (
    <ul className="item-list list-group">
      {property === null ? <CircularProgress /> : property.map((item, index) => {
        return <li className="list-group-item" key={index * 3 + 'r'}> {item.name} </li>
      })}
    </ul>
  );

}

export default ItemList;
